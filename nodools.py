__author__ = "Paulo Antunes"
__copyright__ = "Copyright 2019, nodools"
__credits__ = ["Paulo Antunes", ]
__license__ = "GPL"
__maintainer__ = "Paulo Antunes"
__email__ = "pjmlantunes@gmail.com"

import logging
import schedule
import time
import subprocess
from settings import POOLING_IN_SECONDS


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s',
    filename='/tmp/nodools.log',
    filemode='w'
)


def lto():
    logging.info("Health-check LTO...\n")

    out = subprocess.check_output(
        "docker inspect -f '{{.State.Running}}' public-node", shell=True
    )
    if out.decode().strip() != 'true':

        logging.info("\nNOT GOOD!\n")

        out = subprocess.check_output(
            "cd $HOME/projects/lto-public-node && pwd && git pull && "
            "docker-compose pull && docker-compose down && "
            "docker-compose up -d", shell=True
        )
        logging.debug("{out}\n".format(out=out.decode().strip()))
    else:
        logging.info("ALL GOOD!\n")

    logging.info("Health-check LTO...exit\n")


def idex():
    logging.info("Health-check IDEX...\n")

    out = subprocess.check_output("cd $HOME && pwd && idex status", shell=True)
    if "Staking: online" not in out.decode():

        logging.info("\nNOT GOOD!\n")

        for x in (
            "cd $HOME && pwd && idex stop",
            "cd $HOME && pwd && npm install -g @idexio/idexd-cli",
            "cd $HOME && pwd && idex start",
        ):
            out = subprocess.check_output(x, shell=True)
            logging.debug("{out}\n".format(out=out.decode().strip()))
    else:
        logging.info("ALL GOOD!\n")

    logging.info("Health-check IDEX...exit\n")


if __name__ == '__main__':
    for s in (lto, idex):
        schedule.every(POOLING_IN_SECONDS).seconds.do(s)

    try:
        while True:
            schedule.run_pending()
            time.sleep(POOLING_IN_SECONDS)
            logging.info('Current iteration is DONE')
    except KeyboardInterrupt:
        pass
